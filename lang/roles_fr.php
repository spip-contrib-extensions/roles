<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/roles.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_role' => 'Ajouter un rôle',

	// C
	'choisir_role' => 'Choisir…',

	// E
	'enlever_role' => 'Enlever un rôle',

	// M
	'modifier_roles' => 'Modifier',
	'modifier_roles_title' => 'Modifier les rôles',

	// R
	'role_titre' => 'Rôle',
	'roles_titre' => 'Rôles',

	// S
	'saisir_role' => 'Indiquer un rôle…',
	'saisir_valeur' => 'Valeur',
	'selectionner_roles' => 'Sélectionner le ou les rôles souhaités'
);
